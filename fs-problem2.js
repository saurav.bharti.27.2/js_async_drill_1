
const fs = require('fs-extra')

const path = require('path')

const absolute_file_path = '/home/saurav/mountblue/js_async_drill_1/'



function fsProblem2( text_file_path, where_to_save_filename){

    //Problem 1 - Reading into the given text file

    readFile(text_file_path, (err, text_file_data) => {

        if(err){

            console.error("Error: Unable to read the given file correctly", err)

        }
        else{
            
            // Problem 2 - Converting it to uppercase

            convertToUpperCase(text_file_data, (err, uppercase_data) => {

                if(err){

                    console.error("Error : while converting the file to uppercase characters. ", err)
                    
                }
                else{

                    createANewFile(absolute_file_path, "conversion_to_uppercase.txt", uppercase_data, (err, data) => {

                        if(err){

                            console.error("Error : Can't write into conversion_to_uppercase.txt")
                            

                        }else{

                            let file_path_to_conversion_to_uppercase = "/home/saurav/mountblue/js_async_drill_1/conversion_to_uppercase.txt"

                            saveFilenameToFilenameTxt( where_to_save_filename, file_path_to_conversion_to_uppercase , (err, saved_file_data)=>{
        
                                if(err){
        
                                    console.error("Error:  while saving the filename of Uppercase file created", err)
                                    
                                }
                                else{
                                    
                                    console.log(saved_file_data)

                                    // Problem 3 - Reading the uppercase file and then converting it to a new lowercase file.

                                    readingTheUpperCasedFile('/home/saurav/mountblue/js_async_drill_1/conversion_to_uppercase.txt', (err, uppercase_file_data)=>{

                                        if(err){

                                            console.error("Error: while reading the uppercased file")

                                        }
                                        else{

                                            convertToLowerCase(uppercase_file_data, (err, lowercase_data)=>{

                                                if(err){

                                                    console.error("Error : while converting Uppercased file to lowercase")
                                                    
                                                }
                                                else{
                                                    
                                                    splitTheContentIntoSentences(lowercase_data, (err, splitted_lowercase_sentence)=>{

                                                        if(err){

                                                            console.error("Error : while splitting the content into sentences")
                                                            
                                                        }
                                                        else{

                                                            createANewFile(absolute_file_path, "splitting_after_lowercase.txt", splitted_lowercase_sentence, (err, data)=>{

                                                                if(err){

                                                                    console.error("Error : Cannot write into slitting_after_lowercase.txt ")
                                                                    
                                                                }
                                                                else{
                                                                    let file_path_to_lowercase_split =  '/home/saurav/mountblue/js_async_drill_1/splitting_after_lowercase.txt'

                                                                    saveFilenameToFilenameTxt( where_to_save_filename, file_path_to_lowercase_split , (err, saved_file_data)=>{

                                                                        if(err){

                                                                            console.error("Error : Cannot save the filename in filenames.txt")
                                                                            
                                                                        }
                                                                        else{
                                                                           
                                                                            console.log(saved_file_data)

                                                                            // Problem 4 --- Reading and sorting content of lowercased sentences

                                                                            readFile(file_path_to_lowercase_split, (err, lowercased_file_data) =>{

                                                                                if(err){

                                                                                    console.error("Error : Cannot read the file : "+ file_path_to_lowercase_split)

                                                                                }
                                                                                else{

                                                                                    sortTheContentOfTheFile(lowercased_file_data, 'lowercase', (err, sorted_lowercase_data)=>{

                                                                                        if(err){

                                                                                            console.log("Error: while sorting content of file : "+ file_path_to_lowercase_split, err)

                                                                                        }else{

                                                                                            let file_path_to_sorted_lowercase_split = path.join(absolute_file_path , "sorted_lowercase_split.txt")

                                                                                            createANewFile(absolute_file_path, "sorted_lowercase_split.txt", sorted_lowercase_data, (err, data) => {

                                                                                                if(err){

                                                                                                    console.log("Error : while writing into file : "+ file_path_to_sorted_lowercase_split )

                                                                                                }
                                                                                                else{

                                                                                                    saveFilenameToFilenameTxt(where_to_save_filename, file_path_to_sorted_lowercase_split, (err, saved_file_data) => {

                                                                                                        if(err){

                                                                                                            console.error("Error : while saving filename of : "+ file_path_to_sorted_lowercase_split + " to filenames.txt", err )

                                                                                                        }
                                                                                                        else{

                                                                                                            console.log(saved_file_data)

                                                                                                            //// Problem 4 - reading and sorting the uppercased file now.

                                                                                                            readFile(file_path_to_conversion_to_uppercase, (err, uppercase_file_data) => {

                                                                                                                if(err){

                                                                                                                    console.error("Error : error while reading the file : " + file_path_to_conversion_to_uppercase, err)

                                                                                                                }
                                                                                                                else{


                                                                                                                    sortTheContentOfTheFile(uppercase_file_data, 'uppercase', (err, sorted_uppercase_data)=>{
                                                                                                                        
                                                                                                                        if(err){

                                                                                                                            console.error("Error : error while sorting the file : " + file_path_to_conversion_to_uppercase , err)

                                                                                                                        }else{


                                                                                                                            let file_path_to_sorted_uppercase_split = path.join(absolute_file_path , "sorted_uppercase_split.txt")

                                                                                                                            createANewFile(absolute_file_path, "sorted_uppercase_split.txt", sorted_uppercase_data, (err, data)=>{

                                                                                                                                if(err){

                                                                                                                                    console.error("Error : while writing into file: " + file_path_to_sorted_uppercase_split, err)

                                                                                                                                }else{

                                                                                                                                    saveFilenameToFilenameTxt(where_to_save_filename, file_path_to_sorted_uppercase_split, (err, saved_file_data) => {

                                                                                                                                        if(err){

                                                                                                                                            console.error("Error while saving filename : "+ file_path_to_sorted_uppercase_split, err)

                                                                                                                                        }
                                                                                                                                        else{

                                                                                                                                            console.log(saved_file_data)

                                                                                                                                            // Problem 5 - Reading the filenames.txt and deleting each file

                                                                                                                                            readFile(where_to_save_filename, (err, data_filename_txt) => {

                                                                                                                                                if(err){

                                                                                                                                                    console.error("Error:  while reading the filename : "+ where_to_save_filename, err)

                                                                                                                                                }
                                                                                                                                                else{

                                                                                                                                                    let all_files_to_delete = data_filename_txt.split('\n')


                                                                                                                                                    all_files_to_delete.forEach((file_to_delete) => {

                                                                                                                                                        if(file_to_delete !== ''){


                                                                                                                                                            deleteFile(file_to_delete, (err, delete_data_info) => {
    
                                                                                                                                                                if(err){
    
                                                                                                                                                                    console.error("Error : while deleting the file : "+ file_to_delete , err)
    
                                                                                                                                                                }
                                                                                                                                                                else{
    
                                                                                                                                                                    console.log(delete_data_info)
    
                                                                                                                                                                }
    
                                                                                                                                                            })

                                                                                                                                                        }


                                                                                                                                                    } )

                                                                                                                                                }

                                                                                                                                            } )
                                                                                                                                        }

                                                                                                                                    } )

                                                                                                                                }

                                                                                                                            })


                                                                                                                        }

                                                                                                                    })
                                                                                                                }

                                                                                                            } )

                                                                                                            

                                                                                                        }

                                                                                                    })
                                                                                                }

                                                                                            })


                                                                                        }

                                                                                    })

                                                                                }

                                                                            } )

                                                                        }

                                                                    } )

                                                                }

                                                            })

                                                        }

                                                    })

                                                }

                                            })
                                        }

                                    })
        
                                }
        
                            } )

                        }

                    } )

                    
                }

            })

        }

    } )
    
}

function readFile( absoluteFilePath, callback){
    
    fs.readFile(absoluteFilePath, 'utf-8', (err, data)=>{

        if(!err){
            callback(err, data)
        }else{
            callback(null, data)
        }

    })
    

}

function convertToUpperCase(file_data, callback){

    let uppercased_file_data = ''

    for(let index=0; index < file_data.length; index++){

        if((file_data[index]>= 'a' && file_data[index]<='z')){

            uppercased_file_data += file_data[index].toUpperCase()
        }
        else{
            uppercased_file_data += file_data[index]
        }

    }

    callback(null, uppercased_file_data)
    
}


function createANewFile(absolute_file_path, save_as, data, callback ){

    const file_path = path.join(absolute_file_path, save_as);

    fs.writeFile(file_path, data, (err)=>{

        if(err){
            callback(err, data)
        }
        else{
            callback(null, data)
        }

    } )



}


function readingTheUpperCasedFile(absoluteFilePath, callback ){

    fs.readFile(absoluteFilePath, 'utf-8', (err, data) =>{

        if(err){
            callback(err, data)
        }
        else{
            callback(null, data)
        }

    } )

}

function convertToLowerCase(file_data, callback){

    let lowercased_file_data = ''

    for(let index=0; index < file_data.length; index++){

        if((file_data[index]>= 'A' && file_data[index]<='Z')){

            lowercased_file_data += file_data[index].toLowerCase()
        }
        else{
            lowercased_file_data += file_data[index]
        }

    }

    callback(null, lowercased_file_data)
    
}

function splitTheContentIntoSentences(file_data, callback){

    const splitted_file_data = file_data.split('.')

    let splitted_data_to_return = ''

    splitted_file_data.forEach((each_line, index) =>{

        if(each_line.trim() !== ''){

            splitted_data_to_return += each_line.trim()

            if(index !== splitted_file_data.length - 2){

                splitted_data_to_return += '\n'

            }

        }

    } )

    callback(null, splitted_data_to_return)
}

function sortTheContentOfTheFile(file_data, for_lowercase_or_uppercase, callback){

    if(for_lowercase_or_uppercase === 'lowercase'){

        try{
    
            let sorted_data = file_data.split('\n').sort()
    
            let sorted_string_to_return = sorted_data.join('\n')
    
            callback(null, sorted_string_to_return)
        }
        catch(err){
    
            callback(err, file_data)
    
        }


    }else{

        try{

            let sorted_data  = ''

            let temp_lines = file_data.split('.')

            temp_lines.forEach((line, index, temp_lines) => {

                temp_lines[index] = line.trim()

            })

            temp_lines.sort()

            temp_lines.forEach(each_line => {
                
                let current_line = each_line.trim()

                if(current_line !== ''){

                    sorted_data += current_line
                    
                    sorted_data += '\n'

                }
            } )

            callback(null, sorted_data)

        }
        catch(err){

            callback(err, file_data)

        }

    }

}


function saveFilenameToFilenameTxt(where_to_save_filename, save_as, callback ){

    if(save_as === "/home/saurav/mountblue/js_async_drill_1/conversion_to_uppercase.txt"){

        fs.writeFile(where_to_save_filename, save_as +'\n', (err)=>{

            if(err){
    
                callback(err, null)
    
            }
            else{
    
                callback(null, "Successfully saved the filename : " + where_to_save_filename + save_as)
    
            }

        })

    }else{


        fs.appendFile( where_to_save_filename, save_as + '\n', (err)=>{
    
            if(err){
    
                callback(err, null)
    
            }
            else{
    
                callback(null, "Successfully saved the filename : " + where_to_save_filename + save_as)
    
            }
    
        })

    }
    

}

function deleteFile( absolute_file_path, callback ){

    fs.unlink(absolute_file_path, (err) => {

        if(err){

            callback(err, null)

        }
        else{

            callback(null, "Successfully deleted the file : "+ absolute_file_path)

        }

    } )

}



module.exports = fsProblem2
