const fs = require('fs-extra')

const path = require('path')

function random_json_data_generator(){

    let random_name = ''

    let random_size = Math.floor((Math.random()*100))

    for(let index = 0; index< random_size; index++){

        let now = (Math.floor((Math.random()) * 100)) %26;

        random_name += String.fromCharCode('a'.charCodeAt(0) + now)
    }

    let random_age = Math.floor(Math.random() * 100)

    let random_phone = Math.floor(Math.random() * 100000000000) 



    let random_data = {

        name : random_name,

        age: random_age,

        phone: random_phone

    }
    
    return random_data;

}


function fsProblem1( absolutePathOfRandomDirectory, randomNumberOfFiles ){


    fs.exists(absolutePathOfRandomDirectory, (already_exists)=>{

        if(!already_exists){

            fs.mkdir( absolutePathOfRandomDirectory, (err)=>{

                if(err){

                    console.error("Opertion not permitted to make directory")

                }
                else{

                    let file_count = 0

                    writeFileCallback(absolutePathOfRandomDirectory, randomNumberOfFiles, (err, data)=>{
                
                        if(err){
        
                            console.error("Error while writing into filename - "+ absolutePathOfRandomDirectory,   err)
                            
                        }
                        else{
        
                            console.log(data)
                            
                            file_count += 1
        
                            if(file_count === randomNumberOfFiles){
        
                                deleteRandomFiles(absolutePathOfRandomDirectory, randomNumberOfFiles, (err, data) => {
        
                                    if(err){
        
                                        console.error(err)
        
                                    }
                                    else{
        
                                        console.log(data)
        
                                    }
        
                                })
        
                            }
        
                        }
        
                    })

                }

            } )

        }else{

            let file_count = 0

            writeFileCallback(absolutePathOfRandomDirectory, randomNumberOfFiles, (err, data)=>{
                
                if(err){

                    console.error("Error while writing into filename - "+ absolutePathOfRandomDirectory,   err)
                    
                }
                else{

                    console.log(data)

                    file_count += 1

                    if(file_count === randomNumberOfFiles){

                        deleteRandomFiles(absolutePathOfRandomDirectory, randomNumberOfFiles, (err, data) => {

                            if(err){

                                console.error(err)

                            }
                            else{

                                console.log(data)

                            }

                        })

                    }

                }

            })

        }

    })


    

}



function writeFileCallback(absolutePathOfRandomDirectory, randomNumberOfFiles, callback){
    

    for(let index = 0; index< randomNumberOfFiles; index++){

        const json_data = random_json_data_generator()

        const file_name = `random_file${index+1}.json`

        const file_path = path.join(absolutePathOfRandomDirectory, file_name)

        fs.writeFile(file_path, JSON.stringify(json_data, null, 2 ), (err)=>{

            if(err){
                
                callback("Error while writing in file" + file_path, null )

            }
            else{

                callback(null, "Successfully created the file : " + file_path )

            }          

            
        })


    }


}


function deleteRandomFiles(absolutePathOfRandomDirectory, randomNumberOfFiles, callback){


    for(let index=0; index<randomNumberOfFiles; index++){
        
        let file_name = `random_file${index+1}.json`

        let file_path = path.join(absolutePathOfRandomDirectory, file_name)
        
        if(file_path){

            fs.unlink( file_path, (err) => {

                if(err){
                    
                    callback( "Error in removing file " + file_path, null )
               
                }else{                   

                    callback(null, "Successfully deleted the file : "+ file_path)

                }

            } )

        }

    }

}




module.exports = fsProblem1;